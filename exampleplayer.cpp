// Naija For Life!!!

#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {

     // Keep track of the board
     board = new Board();
     // Keep track of whether we are White or Black
     mySide = side;
     // The opponent is obviously the other color
     (side == BLACK) ? opSide = WHITE : opSide = BLACK;
     // The scoreboard is a copy of the game board in which each cell is given
     // a value. The higher the value, the better off we are if we take it.
     scoreBoard = {0};
     
     // Corner cells are initially the best. Edge cells are quite good. Cells 
     // immediately adjacent to the corners are pretty bad.
     for (int i = 0; i < 8; i++)
     {
        for (int j = 0; j < 8; j++)
        {
            int c = i + 8 * j;
            if ((c == 0) || (c == 7) || (c == 56) || (c == 63))
            {
                scoreBoard[c] = 5;
            }
            else if (c == 1 || c == 6 || c == 8 || c == 15 || c == 48 || c == 55 || c == 57 || c == 62)
            {
                scoreBoard[c] = -1;
            }
            else if (c == 9 || c == 14 || c == 49 || c == 54)
            {
                scoreBoard[c] = -2;
            }
            else if ((c < 7) || (c % 8 == 0) || ((c + 1) % 8 == 0) || (56 < c))
            {
                scoreBoard[c] = 3;
            }
        }
    }           

}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() 
{
    delete board;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opMove, int msLeft) {
     
     board->doMove(opMove, opSide);
     int max = -3;
     Move temp(64, 64);
     
     for (int i = 0; i < 8; i++) 
     {
        for (int j = 0; j < 8; j++) 
        {
           Move move(i, j);
           if ((board->checkMove(&move, mySide)) && (max < scoreBoard[i + 8 * j]))
           {
               max = scoreBoard[i + 8 * j];
               temp = move;
           }
        }
     }
        
     int x = temp.getX();
     int y = temp.getY();
     if (x + y < 127)
     {
	    board->doMove(&temp, mySide);
	    updateBoard();	    
	    updateScoreBoard(&temp);
		Move *res = new Move(temp.getX(), temp.getY());
        return res;
     }
     return NULL;
}
    
void ExamplePlayer::updateScoreBoard(Move *move) 
{

	int c = move->getX() + 8 * move->getY();
	
	// if a corner is occupied by us, the cells immediately vertical and
	// horizontal to it are now the most valuable cells on the board.
	if (c == 0)
    {
        scoreBoard[1] = 4;
        scoreBoard[8] = 4;
    }
    else if (c == 7)
    {
        scoreBoard[6] = 4;
        scoreBoard[15] = 4;
    }
	else if (c == 56)
    {
        scoreBoard[48] = 4;
        scoreBoard[57] = 4;
    }
	else if (c == 63)
    {
        scoreBoard[55] = 4;
        scoreBoard[62] = 4;
    }

}


bool ExamplePlayer::compromisable(int c)
{
    bool res = false;
    int x = c % 8;
    int y = (c - x) / 8;
    if ((x > 0) && (x < 7) && (y > 0) && (y < 7))
    {
        int neighbors[8] = {(x - 1) + 8 * (y - 1),
                            x + 8 * (y - 1),
                            (x + 1) + 8 * (y - 1),
                            (x + 1) + 8 * y,
                            (x + 1) + 8 * (y + 1),
                            x + 8 * (y + 1),
                            (x - 1) + 8 * (y + 1),
                            (x - 1) + 8 * y};
        
        for (int i = 0; i < 4; i++)
        {
            int c0 = neighbors[i];
            int c1 = neighbors[i + 1];
            int c2 = neighbors[i + 2];
            int c3 = neighbors[i + 3];
            int c4 = neighbors[i + 4];
            bool temp = (board->isTaken(c0, mySide) && board->isTaken(c1, mySide) && board->isTaken(c2, mySide)
                && board->isTaken(c3, mySide) && board->isTaken(c4, mySide));
            res = res || temp;
        }
    }
    return res;
}

void ExamplePlayer::update(int c)
{
    int x = c % 8;
    int y = (c - x) / 8;
    int neighbors[8] = {(x - 1) + 8 * (y - 1),
                        x + 8 * (y - 1),
                        (x + 1) + 8 * (y - 1),
                        (x + 1) + 8 * y,
                        (x + 1) + 8 * (y + 1),
                        x + 8 * (y + 1),
                        (x - 1) + 8 * (y + 1),
                        (x - 1) + 8 * y};
    
    for (int i = 0; i < 4; i++)
    {
        int c0 = neighbors[i];
        int c1 = neighbors[i + 1];
        int c2 = neighbors[i + 2];
        int c3 = neighbors[i + 3];
        int c4 = neighbors[i + 4];
        if (scoreBoard[c0] >= 4 &&
            scoreBoard[c1] >= 4 &&
            scoreBoard[c2] >= 4 &&
            scoreBoard[c3] >= 4 &&
            scoreBoard[c4] >= 4 &&
            compromisable(c))
        {
            scoreBoard[c] = 5;
        }
    }
    
}

void ExamplePlayer::updateBoard()
{
    for (int i = 0; i < 64; i++)
    {
        update(i);
    }
}
